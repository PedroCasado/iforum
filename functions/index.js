const functions = require('firebase-functions');
const admin = require('firebase-admin');

const config = {
    apiKey: "AIzaSyA2DBEHMY7qTo2QnUztsY5J5Qfj2IOwj00",
    authDomain: "iforum-d959b.firebaseapp.com",
    databaseURL: "https://iforum-d959b.firebaseio.com",
    projectId: "iforum-d959b",
    storageBucket: "iforum-d959b.appspot.com",
    messagingSenderId: "1015321198973"
  };

admin.initializeApp(config);

exports.isUserRegistered = functions.https.onCall((data, context)=>{
	var status;
	const usersRef = admin.database().ref('/users');
    console.log(data);
	status = Object.keys(usersRef).includes(data.userEmail);
	console.log(status);
	return status;
});
// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });
