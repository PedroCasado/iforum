import React, { Component } from 'react';
import {Link} from 'react-router-dom';

class Organizer extends Component {

  render() {
    var topics = [];
    if (this.props.forum.topics) {
      Object.keys(this.props.forum.topics).forEach(
        (t,i) => { topics.push(
          <Link to={'/'+this.props.id+'/'+t} key={i} className='list-group-item list-group-item-action'>
            <div className='row'>
              <div className='col-5'>
                <span className='font-weight-bold'>{this.props.forum.topics[t].title}</span> por {this.props.forum.topics[t].by}
              </div>
              <div className='col'>
                {this.props.forum.topics[t].content.length > 60 ? 
                  `${this.props.forum.topics[t].content.substring(0,60)}...` :
                  this.props.forum.topics[t].content
                }
              </div>
            </div>
          </Link>
        ) }
      );
    }
    return (
      <div className='row mt-2'>
        <div className='col'>
          <div className='card'>
            <div className='card-header d-flex justify-content-center bg-success text-light'>
              <h4 className='mb-0'>{this.props.forum.name}</h4>
              <div className='ml-auto'>
                <Link to={'/'+this.props.id+'/addPost'} className='btn btn-light text-success py-0'><h3 className='mx-0'>+</h3></Link>
              </div>
            </div>
            <div className='card-body'>
              <div className='list-group'>
                {topics}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Organizer;
