import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {WithContext as ReactTags} from 'react-tag-input';
import firebase from 'firebase';
import Markdown from 'react-markdown';
import {getCurrentUser, appendToActivity} from './authService.js';

const keyCodes = {
  comma: 188,
  semicolon: 59,
  enter: 13
}

const delimiters = [keyCodes.comma, keyCodes.enter, keyCodes.semicolon];

class AddPost extends Component {
  constructor(){
      super();
      this.form = React.createRef();
      this.state = {
        tags: [],
        suggestions: [],
        newPost: {},
        created_at: new Date()
      }
      this.handleDelete = this.handleDelete.bind(this);
      this.handleAddition = this.handleAddition.bind(this);
      this.handleDrag = this.handleDrag.bind(this);

      this.postRef = React.createRef();
  }

  componentWillMount(){
    this.postRef = firebase.database().ref('/react/forums/'+this.props.match.params.forum+'/topics/');
    console.log('addpost');
  }

  handleDelete(i) {
    const { tags } = this.state;
    this.setState({
     tags: tags.filter((tag, index) => index !== i),
    });
  }

  handleAddition(tag) {
    if(this.state.tags.length >= 6){
      alert('Limite máximo de 6 tags.');
      return;
    }
    this.setState(state => ({ tags: [...state.tags, tag] }));
  }

  handleDrag(tag, currPos, newPos) {
      const tags = [...this.state.tags];
      const newTags = tags.slice();

      newTags.splice(currPos, 1);
      newTags.splice(newPos, 0, tag);

      // re-render
      this.setState({ tags: newTags });
  }

  test(e){
    e.preventDefault()
    if(this.form.current)
    var newPost = {
      title: this.form.current[0].value,
      tags: this.state.tags,
      content: this.form.current[2].value,
      created_at: new Date().getTime()
    }
    this.setState({newPost});
    console.log(this.state, firebase.auth().currentUser);
  }

  submit(e){
    e.preventDefault()
    if(this.matches().sts){
      var post = {
        title: this.state.newPost.title,
        tags: this.state.newPost.tags,
        content: this.state.newPost.content,
        created_at: this.state.newPost.created_at,
        by: getCurrentUser().displayName,
      };
      this.postRef.push(post);
      post.at = window.location.href;
      post.action = 'Postou um novo tópico';
      appendToActivity(post);
      this.setState({newPost: {}});
      this.props.history.push('/');
    }
  }

  matches(){
    var output = {sts:false, msg:'Falta'};
    if(!this.state.newPost.title)
      output.msg += ' |título|';
    if(!this.state.newPost.content)
      output.msg += ' |conteúdo|'

    if(output.msg === 'Falta'){
      output.msg += ' nada';
      output.sts = true;
    }
    return output;
  }

  render(){
    const {tags, suggestions} = this.state;
    return (
      <div className='row'>
        <div className='col border-right border-dark'>
          <h2>Novo post</h2>
          <form className='card' ref={this.form} onChange={(e)=>this.test(e)}>
            <div className='card-header'>
              <input className='form-control' id='title' type='text' placeholder='Insira o título da postagem aqui.'/>
            </div>
            <div className='list-group'>
              <div className='list-group-item'>
                <ReactTags classNames={{
                            tagInputField: 'form-control',
                            tag: 'btn btn-primary m-2',
                            remove: 'ml-1 badge badge-pill badge-danger',
                            suggestions: 'form-control'
                          }}
                          tags={tags}
                          suggestions={suggestions}
                          handleDelete={this.handleDelete}
                          handleAddition={this.handleAddition}
                          handleDrag={this.handleDrag}
                          delimiters={delimiters}
                          maxLength='15'
                          placeholder='Insira marcadores... <,> ou <enter> ou <;> para adicionar.'/>
              </div>
            </div>
            <div className='card-body'>
              <textarea className='form-control' placeholder='O conteúdo da postagem vem aqui. As notações de ênfase e estilo deverão ser feitas em Markdown.'></textarea>
            </div>
          </form>
        </div>
        <div className='col'>
          <h2>Previsão</h2>
          <div className='card'>
            <div className='card-header'>{this.state.newPost.title||'Aqui virá o título'}</div>
            <div className='list-group'>
              <div className='list-group-item'>
                <ReactTags classNames={{
                            tag: 'badge badge-pill badge-primary m-2',
                          }}
                          tags={tags.length === 0 ? [{id:'placeholder', text:'Os marcadores virão aqui'}] : tags}
                          readOnly={true}/>
              </div>
            </div>
            <div className='card-body'>
              <Markdown source={this.state.newPost.content||'**O conteúdo da postagem aparecerá aqui**.'}/>
            </div>
            <button className={this.matches().sts ? 'btn btn-success' : 'btn btn-danger disabled'} onClick={(e)=>this.submit(e)}>
              {this.matches().sts ? 'Enviar':this.matches().msg }
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default AddPost;
