import React, { Component } from 'react';
import AddPost from './AddPost.jsx'
import TopicList from './TopicList.jsx';
import * as firebase from 'firebase';

class Topics extends Component {

  constructor(){
    super();
    this.state = {
      topics: []
    }
  }

  componentDidMount(){
    const rootRef = firebase.database().ref().child('react');
    const topicsRef = rootRef.child('topics');
    topicsRef.on('value', snap => {
      this.setState({ topics: snap.val() })
      console.log(snap.val());
    })
  }

    //
  render() {
    return (
      <div className='row'>
        <div className='col'>
          <TopicList topics={this.state.topics}/>
        </div>
      </div>
    );
  }
}

export default Topics;
