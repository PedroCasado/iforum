import React, { Component } from 'react';
import {getCurrentUser, isAlreadyRegistered} from './authService'
import { Link } from 'react-router-dom';
import firebase from 'firebase';
import Markdown from 'react-markdown';

class Register extends Component {

  constructor(){
    super();
    this.state = {
      userdata: {},
      cursoSelecionado: 'nan',
    };
    this.form = React.createRef();
  }

  componentWillMount(){
    firebase.auth().onAuthStateChanged((userdata) => {
      if (userdata) {
        console.log('already connected');
        this.setState({userdata});
      } else {
        console.log('not previously connected');
        this.setState({userdata});
      }
    });
  }

  componentDidMount(){
    let userdata = getCurrentUser();
    this.setState({userdata})
  }

  parseDate(dateObj){
    return `${dateObj.getDate()}/${dateObj.getMonth()}/${dateObj.getFullYear()} às ${dateObj.getHours()}:${dateObj.getMinutes()}`;
  }

  submit(e){
    e.preventDefault();
    getCurrentUser().fbref.set({
      matricula: this.form.current[0].value,
      curso: this.form.current[1].value
    }, (error) => {
      console.log(error);
      this.props.history.push('/');
    })
  }

  render() {
    var userdata = this.state.userdata || getCurrentUser();
    if(!userdata)
      this.props.history.push('/');
    if(userdata){
      isAlreadyRegistered(userdata.email, (result) => {
        console.log(result);
        if(result) this.props.history.push('/');
      })
      return (
        <div className='row'>
          <div className='col-6'>
            <h4>Complete com seus dados</h4>
            <form ref={this.form} onSubmit={(e) => this.submit(e)}>
              <input type='text' className='form-control mb-2' placeholder='Matrícula' required/>
              <select className='form-control mb-2' value={this.state.cursoSelecionado}>
                <option disabled key='nan'>Curso</option>
                <option>Informática</option>
                <option>Geologia</option>
                <option>Edificações</option>
                <option>MSI</option>
                <option>Mineração</option>
              </select>
              <button className='float-right btn btn-success' type='submit'>Enviar</button>
            </form>        
          </div>
        </div>
      );
    } else {
      return (
        <div/>
      )
    }
  }
}

export default Register;
