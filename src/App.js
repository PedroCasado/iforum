import React, {Component} from 'react';
import Navbar from './Navbar.jsx';
import Body from './Body.jsx';

class App extends Component {
  render(){
    return(
      <div>
        <Navbar history={this.props.history} />
        <Body />
      </div>
    );
  }
}

export default App;
