// import {decode} from 'jwt-decode';

import firebase from 'firebase';

const provider = new firebase.auth.GoogleAuthProvider();

const ID_TOKEN_KEY = 'id_token';
const ACCESS_TOKEN_KEY = 'access_token';


export function setCurrentUser(userdata,callback) {
  if(callback) callback();
}

export function getCurrentUser(callback) {
  function encodeKey(s) { return encodeURIComponent(s).replace(new RegExp('\\.','g'),'%2E'); }

  var output = firebase.auth().currentUser;
  if(output) output.fbref = firebase.database().ref('users/'+encodeKey(output.email));
  if(callback) {
  	callback(output);
  	return output;
  } else 
  	return output;
}

export function isAlreadyRegistered(email,callback) {
  function encodeKey(s) { return encodeURIComponent(s).replace(new RegExp('\\.','g'),'%2E'); }

  var user = firebase.auth().currentUser;
  if (user){
    firebase.database().ref('users').once('value').then((ss)=>{
      var output = Object.keys(ss.val()||[]).includes(encodeKey(email)); 
      if(callback) callback(output);
      return output;
    }); 
  }
  return false;
}

export function appendToActivity(payload, callback) {
  function encodeKey(s) { return encodeURIComponent(s).replace(new RegExp('\\.','g'),'%2E'); }

  var user = firebase.auth().currentUser;
  if (user){
    firebase.database().ref('users/'+encodeKey(user.email)+'/activity').push({payload});
    if(callback) callback();
  }
}

export function login(callback) {
  firebase.auth().signInWithPopup(provider).then((result) => {
    // This gives you a Google Access Token. You can use it to access the Google API.
    var token = result.credential.accessToken;
    // The signed-in user info.
    var user = result.user;
    if (callback) callback(token, user);
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    console.log(error);
  });
}

export function logout(callback){
	firebase.auth().signOut().then(function success() {
		console.log('success logout');
		if(callback) callback();
	}, function error(error) {
		console.log('error logout', error);
	})
}
//
// function clearAccessToken() {
//   localStorage.removeItem(ACCESS_TOKEN_KEY);
// }
//
// export function setAccessToken(accessToken) {
//   localStorage.setItem(ACCESS_TOKEN_KEY, accessToken);
// }
//
// export function getAccessToken() {
//   return localStorage.getItem(ACCESS_TOKEN_KEY);
// }
//
// export function logout() {
//   clearAccessToken();
// }
//
// export function isLoggedIn() {
//   const accessToken = getAccessToken();
//   const res = !!accessToken && !isTokenExpired(accessToken);
//   return res;
// }
//
// function getTokenExpirationDate(encodedToken) {
//   const token = decode(encodedToken);
//   if (!token.exp) { return null; }
//
//   const date = new Date(0);
//   date.setUTCSeconds(token.exp);
//
//   return date;
// }
//
// function isTokenExpired(token) {
//   const expirationDate = getTokenExpirationDate(token);
//   return expirationDate < new Date();
// }
