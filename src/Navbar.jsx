import React, {Component} from 'react';
import {Link, withRouter} from 'react-router-dom';
import firebase from 'firebase';
import 'jquery/dist/jquery.js';
import 'popper.js/dist/umd/popper.js'
import 'bootstrap/dist/js/bootstrap.js';
import {getCurrentUser, logout, login} from './authService.js';

class Navbar extends Component {

  constructor(props){
    super(props);
    this.state = {};
    this.provider = {}
  }

  componentWillMount(){
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log('already connected');
        this.setState({user});
      } else {
        console.log('not previously connected');
      }
    });
  }

  test(e){
    e.preventDefault();
    var {history} = this.props;
    console.log(history, this.props);
  }

  login(e){
    e.preventDefault();
    login((token, user)=> {
      console.log(user);
      this.setState({token, user});
      firebase.functions().httpsCallable('isUserRegistered')({userEmail: this.state.user.email}).then((result)=>{
        if(result.data == false) this.props.history.push('/register'); 
      });
    });
  }

  logout(e){
    e.preventDefault();
    logout(() => {
      var user = undefined;
      this.setState({user});
    });
  }

  render(){
    var user = this.state.user || getCurrentUser();
    return (
      <div className='container-fluid px-0 mb-3'>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <Link to='/' className="navbar-brand text-success">IForum</Link>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item active">
                <Link to='/' className="nav-link" >Início <span className="sr-only">(current)</span></Link>
              </li>
              <li className="nav-item">
                <Link to='/home' className="nav-link" >Home <span className="sr-only">(current)</span></Link>
              </li>
              <li className="nav-item">
                <Link to='/addForum' className="nav-link" >Novo Forum <span className="sr-only">(current)</span></Link>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              {user ?
                (<form className='form-inline mr-2'>
                  <div className='btn-group'>
                    <button className='btn btn-success' onClick={(e) => this.test(e)}>{user.displayName}</button>
                    <button className='btn btn-danger' onClick={(e) => this.logout(e)}>Sair</button>
                  </div>
                </form>) : 
                (<button className='btn btn-success mr-2' onClick={(e) => this.login(e)}>Login</button>)
              }
              <div className='input-group'>
                <input className="form-control" type="search" placeholder="Search" aria-label="Search"/>
                <span className='input-group-append'>
                  <button className="btn btn-success my-2 my-sm-0" type="submit">Search</button>
                </span>
              </div>
            </form>
          </div>
        </nav>
      </div>
    );
  }
}

export default withRouter(Navbar);
