import React, {Component} from 'react';
import firebase from "firebase";
import {getCurrentUser} from './authService';

class  Home extends Component {
  constructor() {
    super();
    this.state = {
      userdata: undefined
    }
  }

  componentWillMount(){
    firebase.auth().onAuthStateChanged((userdata) => {
      if (userdata) {
        console.log('already connected');
        this.setState({userdata});
      } else {
        console.log('not previously connected');
        this.setState({userdata});
      }
    });
  }

  componentDidMount(){
    let userdata = getCurrentUser();
    this.setState({userdata})

    if(getCurrentUser())getCurrentUser().fbref.child('activity').on('value',(activity)=>{
      this.setState({activity: activity.val()});
      console.log(this.state);
    });
  }
  parseDate(dateObj){
    return `${dateObj.getDate()}/${dateObj.getMonth()}/${dateObj.getFullYear()} às ${dateObj.getHours()}:${dateObj.getMinutes()}`;
  }
  render(){
    var userdata = this.state.userdata || getCurrentUser();
    var activity = this.state.activity;
    if(!userdata)
      this.props.history.push('/');
    if(userdata){
      var activityelm = [];
      if (activity)
        Object.keys(activity).forEach((key)=>{
          activityelm.push(
            <a class="list-group-item list-group-item-action" href="#list-item-2">
              <div className='col'>
                {activity[key].payload.action}
              </div>
              <div className='col'>

              </div>
              <div className='col'>
              </div> {this.parseDate(new Date(activity[key].payload.created_at))}
            </a>);
        });
      return (<div>
        <div className='row'>
          <div className='col'>
            <div className='card h5 p-2'>
              <div className='row'>
                <div className='col-2'>
                  <img src={userdata.photoURL} width="200" height="auto" alt="..." class="rounded-circle img-fluid"/>
                </div>
                <div className='col my-auto'>
                  <h2>{userdata.displayName}</h2>
                  <p className="text-muted">{userdata.email}</p>
                </div>
                </div>
            </div>
            </div>
        </div>
        <div className='row'>
        <div className='col'>
        <div id="list-example" class="list-group">
          <a class="list-group-item list-group-item d-block p-2 bg-success text-white font-weight-bold" href="#list-item-1">ATIVIDADES:</a>
            {activityelm}
        </div>
        </div>
        </div>
        </div>
      )} else {
        return (
          <div></div>
        );
      }
  }
}

export default Home;
