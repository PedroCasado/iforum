import React, {Component} from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  Switch
} from "react-router-dom";

import {getCurrentUser} from './authService.js'

import { Breadcrumbs } from 'react-breadcrumbs';
import Welcome from './Welcome.jsx';
import Topics from './Topics.jsx';
import Topic from './Topic.jsx';
import AddPost from './AddPost.jsx';
import AddForum from './AddForum.jsx';
import Register from './Register.jsx';
import Home from './Home.jsx';

const routes = [
  {
    path: '/:forum/:post',
    component: Topic
  },
]

const privateRoutes = [
  {
    path: '/:forum/addPost',
    component: AddPost
  },
  {
    path: '/addForum',
    component: AddForum
  },
]
class Body extends Component {
  constructor(){
    super();
    this.state = {
      breadcrumb: []
    }
  }

  componentDidMount(){
    getCurrentUser();
  }

  render(){
    const routesElm = routes.map(
      (r, idx) => { return ( <Route path={r.path} component={r.component}/>) }
    );
    const privateRoutesElm = privateRoutes.map(
      (pr, idx) => { return ( <PrivateRoute path={pr.path} component={pr.component}/>) }
    );
    return (
      <div className='container'>
        <Switch>
          <Route exact path='/' component={Welcome}/>
          <Route path='/home' component={Home}/>
          <Route path='/register' component={Register}/>
          {privateRoutesElm}
          {routesElm}
        </Switch>
      </div>
    );
  }
}

const NoMatch = ({ location }) => (
  <div className="row justify-content-center">
    <div className="col-sm-6 col-md-6">
      <h2 className="text-danger">Ooops...</h2>
      <h4>
        A página <code>{location.pathname}</code> não existe.
      </h4>
      Volte para <Link to="/">o início.</Link>
    </div>
  </div>
);

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
  <Route
    {...rest}
    render={props =>
      getCurrentUser() ? (
        <Component {...props} />
      ) : (
      <div className='row my-auto'>
        <div className='mx-auto'>
          <h1>Você não tem permissão. Logue-se.</h1>
        </div>
      </div>
      )
    }/>)
}

export default Body;
