import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';

import firebase from 'firebase';
var config = {
    apiKey: "AIzaSyA2DBEHMY7qTo2QnUztsY5J5Qfj2IOwj00",
    authDomain: "iforum-d959b.firebaseapp.com",
    databaseURL: "https://iforum-d959b.firebaseio.com",
    projectId: "iforum-d959b",
    storageBucket: "iforum-d959b.appspot.com",
    messagingSenderId: "1015321198973"
  };
firebase.initializeApp(config);

ReactDOM.render((
  <BrowserRouter>
    <App />
  </BrowserRouter>
), document.getElementById('root'));
registerServiceWorker();
