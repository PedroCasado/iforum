import React, { Component } from 'react';
import Topic from './Topic';

class TopicList extends Component {

  render() {
    var topics = [];
    
    Object.keys(this.props.topics).forEach(
      (t,i) => { console.log(this.props.topics[t]); topics.push(<Topic key={i} values={this.props.topics[t]}/>) }
    );
    return (
      <div>
        {topics}
      </div>
    );
  }
}

export default TopicList;
