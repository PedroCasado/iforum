import React, { Component } from 'react';
import {getCurrentUser, appendToActivity} from './authService'
import { Link } from 'react-router-dom';
import firebase from 'firebase';
import Markdown from 'react-markdown';

class Topic extends Component {

  constructor(){
    super();
    this.state = {
      topic: {}
    };
    this.topicRef = React.createRef();
    this.comment= React.createRef();
  }

  componentDidMount(){
    var forum = (this.props.match.params.forum);
    var id = (this.props.match.params.post);
    this.topicRef = firebase.database().ref('/react/forums/'+forum+'/topics/'+id);
    this.commentsRef = firebase.database().ref('/react/forums/'+forum+'/topics/'+id+'/comments');
    this.commentsRef.on('value', (snap) => {
      this.setState({comments: snap.val()});
      console.log(this.state);
    });
    this.topicRef.on('value', (snap) => {
      this.setState({topic: snap.val()});
      console.log(this.state);
    });
  }

  parseDate(dateObj){
    return `${dateObj.getDate()}/${dateObj.getMonth()}/${dateObj.getFullYear()} às ${dateObj.getHours()}:${dateObj.getMinutes()}`;
  }

  updatecomment(e){
    this.setState({comment: this.comment.current.value});
  }


  submitcomment(e){
    var comment = {
      content: this.state.comment,
      created_at: new Date().getTime(),
      by: getCurrentUser().displayName,
    }
    this.commentsRef.push(comment);
    comment.at = window.location.href;
    comment.action = 'Comentou o tópico '+this.state.topic.title;
    appendToActivity(comment);
    this.comment.current.value = '';
  }

  render() {
    if(this.state.comments){
    var comments = [];
    Object.keys(this.state.comments).forEach(
      (c,i) => {
          comments.push(
            <div className='card mb-1'>
            <div className='card-header'>{this.state.comments[c].by} comentou às {this.parseDate(new Date(this.state.comments[c].created_at))}</div>

          <div className='card-body'>{this.state.comments[c].content}</div>
            </div>);
      }
    );
  }

    if(this.state.topic.tags)
      var tags = this.state.topic.tags.map(
        (tag,idx) => {
          return (<span className='badge badge-pill badge-success'>{tag.text}</span>);
        }
      );
    var user = getCurrentUser();
    console.log(this.state);
    return (
      <div>
      <div className='card h5'>
        <div className='card-header'>
          <div className='row'><h2 className='mx-2 my-0'>{this.state.topic.title}</h2></div>
        </div>
        <div className='list-group list-group-flush'>
          <div className='list-group-item'>
            <div className='row mx-1'>{tags}</div>
          </div>
          <div className='list-group-item'>
            <div className='row mx-1'><Link to='/users/1230912309128390'>Por {this.state.topic.by} em {this.state.topic.created_at ? this.parseDate(new Date(this.state.topic.created_at)) : ''}</Link></div>
          </div>
        </div>
        <div className='card-body'>
          <Markdown source={this.state.topic.content}/>
        </div>
      </div>
      {comments}
      {user? (<div className='card h5'>
        <div className='card-header'>
          <div className='row'><h2 className='mx-2 my-0'>Responda esse tópico</h2></div>
        </div>
        <div className='card-body'>
          <textarea ref={this.comment} onChange={(e)=>this.updatecomment(e)} className='form-control'></textarea>
          <button onClick={(e)=>this.submitcomment(e)} className='btn btn-success mt-2'>Enviar</button>
        </div>
      </div>) : (<div></div>)}
      </div>
    );
  }
}

export default Topic;
