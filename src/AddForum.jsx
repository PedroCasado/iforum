import React, {Component} from 'react';
import firebase from 'firebase';

class AddForum extends Component {
  constructor() {
      super();
      this.state = {
        canCreate: false
      }
      this.form = React.createRef();

      this.forumRef = React.createRef();
  }

  componentWillMount(){
    this.forumRef = firebase.database().ref('/react/forums');
  }

  test(e){
    if(this.form.current[0].value)
      this.setState({canCreate:true});
    else
      this.setState({canCreate:false});
  }

  submit(e){
    e.preventDefault();
    if(this.form.current[0].value){
      var newForum = {
        name: this.form.current[0].value,
        started_at: (new Date())
      }
      this.forumRef.push(newForum);
    }
    console.log(newForum);
  }

  render(){
    return (
      <div className='row'>
        <div className='col-6'>
          <form ref={this.form} onChange={(e)=>this.test(e)}>
            <input className='form-control' placeholder='Nome do forum'/>
            <button className={this.state.canCreate ? 'btn btn-outline-primary' : 'btn btn-outline-danger disabled'}
               onClick={(e)=>this.submit(e)}>
               Criar
             </button>
          </form>
        </div>
      </div>
    );
  }
}

export default AddForum;
