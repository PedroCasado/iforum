import React, { Component } from 'react';
import Organizer from './Organizer.jsx'
import firebase from 'firebase';

class Welcome extends Component {
  constructor(){
    super();
    this.state = {
      forums : []
    }
  }

  componentDidMount(){
    try { 
      console.log('opa');
      firebase.database().ref('/react/forums').on('value', (snap) => {
      var forums = []
      Object.keys(snap.val()).forEach(
        (key, index) => {forums.push(<Organizer key={key} id={key} forum={snap.val()[key]}/>)}
      )
      this.setState({forums});
      console.log(this.state.forums);
    });
    } catch (error) {
      console.log(error);
    } 
  }

  componentWillUnmount(){
    console.log('epa')
  }

  render() {
    var elm = this.state.forums.map(
      (e) => { return e }
    )
    return (
      <div className='row'>
        <div className='col'>
          {elm}
        </div>
      </div>
    );
  }
}

export default Welcome;
